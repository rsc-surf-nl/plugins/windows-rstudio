  
$LOGFILE = "c:\logs\plugin-windows-rstudio-install.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Start windows-rstudio"

  Write-Log 'Download installers'

  $ProgressPreference = 'SilentlyContinue'  # fast Invoke-WebRequest

  $dir = "c:\temp"

  New-Item -ItemType Directory -Path $dir 

  $urlRStudio = [System.Environment]::GetEnvironmentVariable('RSTUDIO_URL')
  $outputRStudio = "$dir\RStudioSetup.exe"

  $wcRStudio = New-Object System.Net.WebClient
  $wcRStudio.DownloadFile($urlRStudio, $outputRStudio) 

  $urlR = [System.Environment]::GetEnvironmentVariable('R_URL') 
  $outputR = "$dir\R-win.exe"
  Invoke-WebRequest -Uri $urlR -OutFile $outputR

  $urlRtools = [System.Environment]::GetEnvironmentVariable('RTOOLS_URL')
  $outputRtools = "$dir\R-tools.exe"
  $wcRtools = New-Object System.Net.WebClient
  $wcRtools.DownloadFile($urlRtools, $outputRtools)

  $dirRinstall = 'c:/R'

  Write-Log 'Run installers'

  $argsRStudio = @("/S")
  Start-Process -Filepath $outputRStudio -ArgumentList $argsRStudio -Wait

  $argsR = @("/SILENT", "/SUPPRESSMSGBOXES", "/SP-", "/DIR=$dirRinstall")
  Start-Process -FilePath $outputR -ArgumentList  $argsR -Wait
  
  $argsRtools = @("/SILENT", "/SUPPRESSMSGBOXES", "/SP-") 
  Start-Process -FilePath $outputRtools -ArgumentList $argsRtools -Wait

  Write-Log 'Add R bin folder to path'

  [Environment]::SetEnvironmentVariable(
    "Path",
    [Environment]::GetEnvironmentVariable("Path", [EnvironmentVariableTarget]::Machine) + ";$dirRinstall/bin",
    [EnvironmentVariableTarget]::Machine)
  
  Write-Log 'Remove installers'

  Remove-Item  $outputRStudio
  Remove-Item  $outputR
  Remove-Item  $outputRtools

  Write-Log 'Create shortcut'


  $WScriptShell = New-Object -ComObject WScript.Shell
  $Shortcut = $WScriptShell.CreateShortcut('C:\Users\Public\Desktop\R Studio.lnk')
  $Shortcut.TargetPath = "c:\Program Files\rstudio\rstudio.exe"

  $Shortcut.Save()


  Write-Log "End windows-rstudio"

}

Main     
 
